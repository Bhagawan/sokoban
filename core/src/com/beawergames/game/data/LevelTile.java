package com.beawergames.game.data;

public class LevelTile {
    private final Point texturePointer;
    private final char mapObject;

    public LevelTile(Point point, char object) {
        texturePointer = point;
        mapObject = object;
    }

    public Point getTexturePointer() {
        return texturePointer;
    }

    public char getMapObject() {
        return mapObject;
    }
}
