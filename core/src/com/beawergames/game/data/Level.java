package com.beawergames.game.data;

import java.util.ArrayList;
import java.util.List;


public class Level {
    private LevelTile[][] level;
    private int width, height;

    public Level(List<String> map) {
        fixMap(map);
        if(map.size() > 0) {
            this.width = map.get(0).length();
            this.height = map.size();
            level = new LevelTile[width][height];
            for(int i = 0; i < width; i++) {
                for(int j = 0; j < height; j++) {
                    String s = "";
                    if(j == 0) {
                        s+="www";
                    } else {
                        s += i - 1 >= 0 ? wOrNot(map.get(j - 1).charAt(i - 1)) : "w";
                        s += wOrNot(map.get(j - 1).charAt(i));
                        s += i + 1 < width ? wOrNot(map.get(j - 1).charAt(i + 1)) : "w";
                    }
                    s += i - 1 >= 0 ? wOrNot(map.get(j).charAt(i - 1)) : "w";
                    s += wOrNot(map.get(j).charAt(i));
                    s += i + 1 < width ? wOrNot(map.get(j).charAt(i + 1)) : "w";
                    if(j == height - 1) {
                        s+="www";
                    } else {
                        s += i - 1 >= 0 ? wOrNot(map.get(j + 1).charAt(i - 1)) : "w";
                        s += wOrNot(map.get(j + 1).charAt(i));
                        s += i + 1 < width ? wOrNot(map.get(j + 1).charAt(i + 1)) : "w";
                    }
                    level[i][j] = new LevelTile(getTexturePointer(s), map.get(j).charAt(i));
                }
            }
        }
    }

    public void addFromBothSides(int rows, int columns) {
        LevelTile[][] newLevel = new LevelTile[width + columns * 2][height + rows * 2];

        width += columns * 2;
        height += rows * 2;
        for(int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                if(i < rows || i >= height - rows || j < columns || j >= width - columns) {
                    newLevel[j][i] = new LevelTile(new Point(1,1), 'w');
                } else newLevel[j][i] = level[j - columns][i - rows];
            }
        }
        level = newLevel;
    }

    public Point getTextureAt(int x, int y) {
        if(x >= 0 && x < width && y >= 0 && y < height) return level[x][y].getTexturePointer();
        else return new Point(1,1);
    }

    public char getObjectAt(int x, int y) {
        if(x >= 0 && x < width && y >= 0 && y < height) return level[x][y].getMapObject();
        else return 'w';
    }

    public Point getBaseCharLocation() {
        for(int h = 0; h < height; h++) {
            for(int w = 0; w < width; w++) {
                if(level[w][h].getMapObject() == 'p') {
                    return new Point(w, h);
                }
            }
        }
        return null;
    }

    public List<Point> getBaseBoxesLocation() {
        ArrayList<Point> boxes = new ArrayList<>();
        for(int h = 0; h < height; h++) {
            for(int w = 0; w < width; w++) {
                if(level[w][h].getMapObject() == 'b' || level[w][h].getMapObject() == '*' ) {
                    boxes.add(new Point(w, h));
                }
            }
        }
        return boxes;
    }

    public List<Point> getBaseTargetsLocations() {
        ArrayList<Point> targets = new ArrayList<>();
        for(int h = 0; h < height; h++) {
            for(int w = 0; w < width; w++) {
                if(level[w][h].getMapObject() == 'o' || level[w][h].getMapObject() == '*' ) {
                    targets.add(new Point(w, h));
                }
            }
        }
        return targets;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    private char wOrNot(char c) {
        if(c == 'w') return c;
        else return ' ';
    }

    private void fixMap(List<String> map) {
        int length = 0;
        for(String s: map) if(s.length() > length) length = s.length();
        for(int i = 0; i < map.size(); i++) {
            StringBuilder sBuilder = new StringBuilder(map.get(i));
            boolean b = true;
            while(sBuilder.length() < length) sBuilder.append('w');
            for(int c = 0; c < sBuilder.length(); c++) {
                if(sBuilder.charAt(c) == ' ' && b) sBuilder.setCharAt(c, 'w');
                else b = false;
            }

            map.set(i, sBuilder.toString());
        }
    }

    private Point getTexturePointer(String code) {
        Point p;
        if(code.charAt(4) == ' ') return new Point(3,0);
        switch (code) {
            case "wwwwwwwww":
                p = new Point(1,1);
                break;
            case "    ww ww":
                p = new Point(0,0);
                break;
            case "   wwwwww":
            case "w  wwwwww":
            case "  wwwwwww":
                p = new Point(1,0);
                break;
            case "   ww ww ":
                p = new Point( 2,0);
                break;
            case " ww ww ww":
            case "www ww ww":
            case " ww wwwww":
                p = new Point(0,1);
                break;
            case "ww ww ww ":
            case "wwwww ww ":
            case "ww ww www":
                p = new Point(2,1);
                break;
            case " ww ww   ":
                p = new Point(0,2);
                break;
            case "wwwwww   ":
            case "wwwwwww  ":
            case "wwwwww  w":
                p = new Point(1,2);
                break;
            case "ww ww    ":
                p = new Point(2,2);
                break;
            case "         ":
                p = new Point(3,0);
                break;
            case "wwwwwwww ":
                p = new Point(0,15);
                break;
            case "wwwwww ww":
                p = new Point(1,15);
                break;
            case "ww wwwwww":
                p = new Point(0,16);
                break;
            case " wwwwwwww":
                p = new Point(1,16);
                break;
            case "    ww w ":
                p = new Point(0,3);
                break;
            case "   www   ":
                p = new Point(1,3);
                break;
            case "   ww  w ":
                p = new Point(2,3);
                break;
            case " w  w  w ":
                p = new Point(0,4);
                break;
            case "    w    ":
                p = new Point(1,4);
                break;
            case " w  ww   ":
                p = new Point(0,5);
                break;
            case " w ww    ":
                p = new Point(2,5);
                break;
            case "    w  w ":
                p = new Point(1,6);
                break;
            case "    ww   ":
                p = new Point(0,7);
                break;
            case " w www w ":
                p = new Point(1,7);
                break;
            case "   ww    ":
                p = new Point(2,7);
                break;
            case " w  w    ":
                p = new Point(1,8);
                break;
            case "   www w ":
                p = new Point(1,9);
                break;
            case " w  ww w ":
                p = new Point(0,10);
                break;
            case " w ww  w ":
                p = new Point(2,10);
                break;
            case " w www   ":
                p = new Point(1,11);
                break;
            case "wwwwww w ":
                p = new Point(1,12);
                break;
            case "ww wwwww ":
                p = new Point(0,13);
                break;
            case " wwwww ww":
                p = new Point(2,13);
                break;
            case " w wwwwww":
                p = new Point(1,14);
                break;
            case " ww ww w ":
                p = new Point(0,17);
                break;
            case "ww ww  w ":
                p = new Point(1,17);
                break;
            case " w  ww ww":
                p = new Point(0,18);
                break;
            case " w ww ww ":
                p = new Point(1,18);
                break;
            case "   wwwww ":
                p = new Point(0,19);
                break;
            case "   www ww":
                p = new Point(1,19);
                break;
            case "ww www   ":
                p = new Point(0,20);
                break;
            case " wwwww   ":
                p = new Point(1,20);
                break;
            case " w www ww":
                p = new Point(0,21);
                break;
            case " w wwwww ":
                p = new Point(1,21);
                break;
            case " wwwww w ":
                p = new Point(0,22);
                break;
            case "ww www w ":
                p = new Point(1,22);
                break;
            case " wwwwwww ":
                p = new Point(0,23);
                break;
            case "ww www ww":
                p = new Point(1,23);
                break;
            default:
                p = defPoint(code);
                break;
        }
        return p;
    }

    private Point defPoint(String code) {
        boolean top = code.charAt(1) == 'w';
        boolean bottom = code.charAt(7) == 'w';
        boolean left = code.charAt(3) == 'w';
        boolean right = code.charAt(5) == 'w';

        if(top) {
            if(left) {
                if(right) {
                    if(bottom) return new Point(1,7);
                    else {
                        if(code.charAt(0) == 'w' && code.charAt(2) == 'w') return new Point(1,2);
                        else return new Point(1,11);
                    }
                } else {
                    if(bottom) {
                        if(code.charAt(0) == 'w' && code.charAt(6) == 'w') return new Point(2,1);
                        else return new Point(2,10);
                    }
                    else {
                        if(code.charAt(0) == 'w') return new Point(2,2);
                        else return new Point(2,5);
                    }
                }
            } else {
                if(right) {
                    if(bottom) {
                        if(code.charAt(2) == 'w' && code.charAt(8) == 'w') return new Point(0,1);
                        else return new Point(0,10);
                    }
                    else {
                        if(code.charAt(2) == 'w') return new Point(0,2);
                        else return new Point(0,5);
                    }
                } else {
                    if(bottom) return new Point(0,4);
                    else return new Point(1,8);
                }
            }
        } else {
            if(left) {
                if(right) {
                    if(bottom) {
                        if(code.charAt(6) == 'w' && code.charAt(8) == 'w') return new Point(1,0);
                        else return new Point(1,9);
                    }
                    else return new Point(1,3);
                } else {
                    if(bottom) {
                        if(code.charAt(6) == 'w') return new Point(2,0);
                        else return new Point(2,3);
                    }
                    else return new Point(2,7);
                }
            } else {
                if(right) {
                    if(bottom) {
                        if(code.charAt(8) == 'w') return new Point(0,0);
                        else return new Point(0,3);
                    }
                    else return new Point(0,7);
                } else {
                    if(bottom) return new Point(1,6);
                    else return new Point(1,4);
                }
            }
        }
    }
}
