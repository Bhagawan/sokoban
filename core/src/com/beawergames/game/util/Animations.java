package com.beawergames.game.util;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.Array;
import com.beawergames.game.Assets;

public class Animations {
    public static Animation<TextureRegion> geyser_anim;

    public static void create() {
        Array<TextureRegion> geyser_frames = new Array<>();
        geyser_frames.add(Assets.levelTiles[3][1]);
        geyser_frames.add(Assets.levelTiles[3][2]);
        geyser_frames.add(Assets.levelTiles[3][3]);
        geyser_anim = new Animation<>(0.25f, geyser_frames, Animation.PlayMode.LOOP);
    }
}
