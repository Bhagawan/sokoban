//For tileset credits to SilverDeoxys563, ronTheCrab.
package com.beawergames.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTileSets;

public class Assets {
    public static Texture tileset, character, winScreen, ui_retry, ui_exit, stone;
    public static TextureRegion[][] levelTiles;

    public static Texture loadTexture (String file) {
        return new Texture(Gdx.files.internal(file));
    }

    public static void load() {
        character = loadTexture("human.png");
        winScreen = loadTexture("level_completed.png");
        stone = loadTexture("stone.png");
        tileset = loadTexture("tileset.png");
        ui_retry = loadTexture("retry_btn.png");
        ui_exit = loadTexture("close_btn.png");
        levelTiles = splitTileset(tileset);
    }

    private static TextureRegion[][] splitTileset(Texture texture) {
        int width = texture.getWidth() / 25;
        int height = texture.getHeight() / 25;

        TextureRegion[][] tiles = new TextureRegion[width][height];

        for(int i = 0; i < width; i++) {
            for(int j = 0; j < height; j++) {
                tiles[i][j] = new TextureRegion(texture, 1 + i * 25, 1 + j * 25, 24,24);
            }
        }
        return tiles;
    }

}
