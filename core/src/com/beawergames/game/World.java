package com.beawergames.game;

import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.beawergames.game.components.AnimationComponent;
import com.beawergames.game.components.BoxComponent;
import com.beawergames.game.components.LevelComponent;
import com.beawergames.game.components.PlayerComponent;
import com.beawergames.game.components.TargetComponent;
import com.beawergames.game.components.TextureComponent;
import com.beawergames.game.components.TransformComponent;
import com.beawergames.game.components.UIComponent;
import com.beawergames.game.data.Level;
import com.beawergames.game.data.Point;
import com.beawergames.game.util.Animations;

import java.util.List;

public class World {
    private Engine engine;
    private Level level;

    public World(Engine engine, Level level) {
        this.engine = engine;
        this.level = level;
    }

    public void create() {
        Entity levelEntity = new Entity();
        LevelComponent levelComponent = new LevelComponent(level);
        int size = Game.SCREEN_HEIGHT < Game.SCREEN_WIDTH ? Game.SCREEN_HEIGHT / levelComponent.level.getHeight() : Game.SCREEN_WIDTH / levelComponent.level.getWidth();
        levelComponent.squareSize = size;
        float xOffset = (float) (Game.SCREEN_WIDTH - size * levelComponent.level.getWidth()) / 2;
        float yOffset = (float) (Game.SCREEN_HEIGHT - size * levelComponent.level.getHeight()) / 2;
        int r = 0, c = 0;
        if(xOffset > 0) c = (int) Math.ceil(xOffset / size);
        if(yOffset > 0) r = (int) Math.ceil(yOffset / size);
        if(r > 0 || c > 0) {
            levelComponent.level.addFromBothSides(r, c);
            levelComponent.xOffset = (float) (Game.SCREEN_WIDTH - size * levelComponent.level.getWidth()) / 2;
            levelComponent.yOffset = (float) (Game.SCREEN_HEIGHT - size * levelComponent.level.getHeight()) / 2;
        }

        createChar(levelComponent.level.getBaseCharLocation(), (int) levelComponent.squareSize, levelComponent.xOffset, levelComponent.yOffset);
        createBoxes(levelComponent.level.getBaseBoxesLocation(), (int) levelComponent.squareSize, levelComponent.xOffset, levelComponent.yOffset);
        createTargets(levelComponent.level.getBaseTargetsLocations(), (int) levelComponent.squareSize, levelComponent.xOffset, levelComponent.yOffset);
        createUI();

        levelEntity.add(levelComponent);
        engine.addEntity(levelEntity);
    }

    private void createChar(Point p, int squareSize, float xOffset, float yOffset) {
        Entity character = new Entity();

        TextureComponent tc = new TextureComponent(Assets.character);
        TransformComponent trc = new TransformComponent((p.x + 0.5f) * squareSize + xOffset, Game.SCREEN_HEIGHT - (p.y + 0.5f) * squareSize - yOffset, 1);
        trc.setSize(squareSize, squareSize);
        PlayerComponent pc = new PlayerComponent();

        character.add(pc);
        character.add(tc);
        character.add(trc);
        engine.addEntity(character);
    }

    private void createBoxes(List<Point> boxes, int squareSize, float xOffset, float yOffset) {
        for(Point p : boxes) {
            Entity box = new Entity();
            TextureComponent tc = new TextureComponent(Assets.stone);
            TransformComponent trc = new TransformComponent((p.x + 0.5f) * squareSize + xOffset, Game.SCREEN_HEIGHT - (p.y + 0.5f) * squareSize - yOffset, 1);
            trc.setSize(squareSize, squareSize);

            box.add(new BoxComponent());
            box.add(tc);
            box.add(trc);
            engine.addEntity(box);
        }
    }

    private void createTargets(List<Point> targets, int squareSize, float xOffset, float yOffset) {
        for(Point p : targets) {
            Entity target = new Entity();
            TextureComponent tc = new TextureComponent(Assets.levelTiles[3][1]);
            TransformComponent trc = new TransformComponent((p.x + 0.5f) * squareSize + xOffset, Game.SCREEN_HEIGHT - (p.y + 0.5f) * squareSize - yOffset, 0);
            trc.setSize(squareSize, squareSize);

            AnimationComponent ac = new AnimationComponent(Animations.geyser_anim);

            target.add(tc);
            target.add(trc);
            target.add(ac);
            target.add(new TargetComponent());
            engine.addEntity(target);
        }
    }

    private void createUI() {
        Entity exit = new Entity();
        TextureComponent tc = new TextureComponent(Assets.ui_exit);
        TransformComponent trc = new TransformComponent(Game.SCREEN_WIDTH - 35, Game.SCREEN_HEIGHT - 35, 2);
        trc.setSize(50, 50);


        exit.add(tc);
        exit.add(trc);
        exit.add(new UIComponent(UIComponent.ACTION_EXIT));
        engine.addEntity(exit);

        Entity retry = new Entity();
        TextureComponent tcR = new TextureComponent(Assets.ui_retry);
        TransformComponent trcR = new TransformComponent(Game.SCREEN_WIDTH / 2.0f, Game.SCREEN_HEIGHT - 35, 2);
        trcR.setSize(50, 50);


        retry.add(tcR);
        retry.add(trcR);
        retry.add(new UIComponent(UIComponent.ACTION_RETRY));
        engine.addEntity(retry);
    }

    public void restart() {
        Entity levelEntity = engine.getEntitiesFor(Family.all(LevelComponent.class).get()).first();
        engine.removeAllEntities();

        LevelComponent levelComponent = levelEntity.getComponent(LevelComponent.class);
        createChar(levelComponent.level.getBaseCharLocation(), (int) levelComponent.squareSize, levelComponent.xOffset, levelComponent.yOffset);
        createBoxes(levelComponent.level.getBaseBoxesLocation(), (int) levelComponent.squareSize, levelComponent.xOffset, levelComponent.yOffset);
        createTargets(levelComponent.level.getBaseTargetsLocations(), (int) levelComponent.squareSize, levelComponent.xOffset, levelComponent.yOffset);
        createUI();

        engine.addEntity(levelEntity);
    }

    public void createWinScreen() {
        Entity winScreen = new Entity();
        TextureComponent tc = new TextureComponent(Assets.winScreen);
        TransformComponent trc = new TransformComponent(Game.SCREEN_WIDTH / 2, Game.SCREEN_HEIGHT / 2, 3);
        trc.setSize(Assets.winScreen.getWidth(), Assets.winScreen.getHeight());
        winScreen.add(tc);
        winScreen.add(trc);
        engine.addEntity(winScreen);
    }

}
