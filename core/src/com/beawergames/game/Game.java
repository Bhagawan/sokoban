package com.beawergames.game;

import com.badlogic.ashley.core.Engine;
import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.ScreenUtils;
import com.beawergames.game.data.Level;
import com.beawergames.game.systems.GeyserAnimSystem;
import com.beawergames.game.systems.MovingSystem;
import com.beawergames.game.systems.RenderLevelSystem;
import com.beawergames.game.systems.RenderSystem;
import com.beawergames.game.systems.TouchSystem;
import com.beawergames.game.util.Animations;

public class Game extends ApplicationAdapter {
	public static final int SCREEN_WIDTH = 480;
	public static final int SCREEN_HEIGHT = 800;
	public static final byte STATE_GAME = 0;
	public static final byte STATE_END = 1;
	private OrthographicCamera camera;
	SpriteBatch batch;
	private Engine engine;
	private World world;
	private TouchSystem tSystem;
	private Level level;
	private LevelCompletedCallback callback;

	public Game(Level level) {
		this.level = level;
	}
	
	@Override
	public void create () {
		engine = new Engine();
		camera = new OrthographicCamera();
		camera.setToOrtho(false, SCREEN_WIDTH, SCREEN_HEIGHT);

		batch = new SpriteBatch();
		Assets.load();
		Animations.create();
		world = new World(engine, level);
		world.create();

		engine.addSystem(new RenderLevelSystem(batch, SCREEN_WIDTH, SCREEN_HEIGHT, 1));
		engine.addSystem(new RenderSystem(batch,10));
		MovingSystem ms = new MovingSystem(8);
		engine.addSystem(ms);
		engine.addSystem(new GeyserAnimSystem(2));

		tSystem = new TouchSystem(world,7);
		engine.addSystem(tSystem);

		ms.setWinCallback(new MovingSystem.OnWinCallback() {
			@Override
			public void onWin() {
				engine.removeSystem(tSystem);
				world.createWinScreen();
				endGame();
			}
		});

		Gdx.input.setInputProcessor(new InputAdapter(){
			@Override
			public boolean touchDown(int screenX, int screenY, int pointer, int button) {
				tSystem.setTouch(screenX, screenY);
				return true;
			}
		});
	}

	@Override
	public void render () {
		ScreenUtils.clear(0, 0, 0, 1);
		camera.update();
		batch.setProjectionMatrix(camera.combined);
		engine.update(Gdx.graphics.getDeltaTime());
	}

	@Override
	public void resize(int width, int height) {
		super.resize(width, height);
	}

	@Override
	public void dispose () {
		batch.dispose();
	}

	private void endGame() {
		if(callback != null) callback.onLevelCompleted();
		new java.util.Timer().schedule(
				new java.util.TimerTask() {
					@Override
					public void run() {
						Gdx.app.exit();
					}
				},
				3000
		);
	}

	public void setCallback(LevelCompletedCallback callback) {
		this.callback = callback;
	}


	public interface LevelCompletedCallback {
		void onLevelCompleted();
	}
}
