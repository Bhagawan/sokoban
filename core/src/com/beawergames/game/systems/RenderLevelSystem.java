package com.beawergames.game.systems;

import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.beawergames.game.Assets;
import com.beawergames.game.components.LevelComponent;
import com.beawergames.game.data.Point;

public class RenderLevelSystem extends IteratingSystem {
    private SpriteBatch batch;
    private ComponentMapper<LevelComponent> lm;
    private float width, height;

    public RenderLevelSystem(SpriteBatch spriteBatch, int width, int height, int priority) {
        super(Family.one(LevelComponent.class).get(), priority);
        lm = ComponentMapper.getFor(LevelComponent.class);
        batch = spriteBatch;
        this.width = width;
        this.height = height;
    }

    @Override
    protected void processEntity(Entity entity, float deltaTime) {
        LevelComponent lc = lm.get(entity);

        int size = (int) lc.squareSize;
        float xOffset = lc.xOffset;
        float yOffset = lc.yOffset;
        batch.begin();
        for(int h = 0; h < lc.level.getHeight(); h++) {
            for(int w = 0; w < lc.level.getWidth(); w++) {
                Point p = lc.level.getTextureAt(w, h);
                TextureRegion tex = Assets.levelTiles[p.x][p.y];
                batch.draw(tex, w * size + xOffset, height - yOffset - h * size - size, size, size);
            }
        }

        batch.end();
    }
}
