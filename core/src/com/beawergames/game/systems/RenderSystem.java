package com.beawergames.game.systems;

import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.Array;
import com.beawergames.game.components.AnimationComponent;
import com.beawergames.game.components.TextureComponent;
import com.beawergames.game.components.TransformComponent;

import java.util.Comparator;

public class RenderSystem extends IteratingSystem {
    private ComponentMapper<TextureComponent> textM;
    private ComponentMapper<TransformComponent> transM;
    private ComponentMapper<AnimationComponent> animM;
    private SpriteBatch batch;
    private Comparator<Entity> comparator;

    private Array<Entity> renderQueue;

    public RenderSystem(SpriteBatch spriteBatch, int priority) {
        super(Family.all(TextureComponent.class, TransformComponent.class).get(), priority);

        this.batch = spriteBatch;
        textM = ComponentMapper.getFor(TextureComponent.class);
        transM = ComponentMapper.getFor(TransformComponent.class);
        animM = ComponentMapper.getFor(AnimationComponent.class);

        renderQueue = new Array<>();

        comparator = new Comparator<Entity>() {
            @Override
            public int compare(Entity entityA, Entity entityB) {
                return (int)Math.signum(transM.get(entityA).pos.z - transM.get(entityB).pos.z);
            }
        };
    }

    @Override
    public void update(float deltaTime) {
        super.update(deltaTime);

        renderQueue.sort(comparator);

        TransformComponent trc;
        TextureComponent txc;
        AnimationComponent animC;

        batch.begin();

        for(Entity entity: renderQueue) {
            trc = transM.get(entity);
            txc = textM.get(entity);
            animC = animM.get(entity);
            if(animC != null) {
                batch.draw(animC.getCurrFrame(deltaTime), trc.pos.x - trc.width * 0.5f, trc.pos.y - trc.height * 0.5f,trc.width * 0.5f, trc.height * 0.5f,
                        trc.width , trc.height, trc.scaleX, trc.scaleY, trc.angle);
            }
            else batch.draw(txc.tex, trc.pos.x - trc.width * 0.5f, trc.pos.y - trc.height * 0.5f,trc.width * 0.5f, trc.height * 0.5f,
                    trc.width , trc.height, trc.scaleX, trc.scaleY, trc.angle);
        }

        batch.end();
        renderQueue.clear();
    }

    @Override
    protected void processEntity(Entity entity, float deltaTime) {
        renderQueue.add(entity);
    }
}
