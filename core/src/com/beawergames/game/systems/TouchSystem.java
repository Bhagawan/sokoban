package com.beawergames.game.systems;

import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.badlogic.ashley.utils.ImmutableArray;
import com.badlogic.gdx.Gdx;
import com.beawergames.game.Game;
import com.beawergames.game.World;
import com.beawergames.game.components.BoxComponent;
import com.beawergames.game.components.LevelComponent;
import com.beawergames.game.components.MoveComponent;
import com.beawergames.game.components.PlayerComponent;
import com.beawergames.game.components.TransformComponent;
import com.beawergames.game.components.UIComponent;

public class TouchSystem extends IteratingSystem {
    private ComponentMapper<TransformComponent> tm;
    private float tX = -1, tY = -1;
    private boolean newTouch = false;
    private float multX, multY;
    private float yOff;

    private ImmutableArray<Entity> level;
    private ImmutableArray<Entity> boxes;
    private ImmutableArray<Entity> uiElements;

    private World world;


    public TouchSystem(World world, int priority) {
        super(Family.all(PlayerComponent.class).get(), priority);
        this.world = world;

        tm = ComponentMapper.getFor(TransformComponent.class);

        multX = Math.abs((float)Game.SCREEN_WIDTH / Gdx.graphics.getWidth());
        multY = Math.abs((float)Game.SCREEN_HEIGHT / Gdx.graphics.getHeight());
        yOff = Gdx.graphics.getHeight();
    }

    @Override
    public void addedToEngine(Engine engine) {
        level = engine.getEntitiesFor(Family.all(LevelComponent.class).get());
        boxes = engine.getEntitiesFor(Family.all(BoxComponent.class).get());
        uiElements = engine.getEntitiesFor(Family.all(UIComponent.class).get());
        super.addedToEngine(engine);
    }

    @Override
    protected void processEntity(Entity entity, float deltaTime) {
        MoveComponent pMc = entity.getComponent(MoveComponent.class);
        if(newTouch && pMc == null) {
            LevelComponent lc = level.first().getComponent(LevelComponent.class);
            TransformComponent tc = tm.get(entity);

            float dX = tX - tc.pos.x;
            float dY = tY - tc.pos.y;

            float newX , newY;

            boolean b = Math.abs(dX) > Math.abs(dY);

            float newDX = b ? lc.squareSize * Math.signum(dX) : 0;
            float newDY = b ? 0 : lc.squareSize * Math.signum(dY);
            newX = tc.pos.x - (tc.pos.x - lc.xOffset) % lc.squareSize + lc.squareSize * 0.5f + newDX;
            newY = tc.pos.y - (tc.pos.y - lc.yOffset) % lc.squareSize + lc.squareSize * 0.5f + newDY;
            Entity box = getBoxAt(newX, newY);
            if(box != null) {
                if(pushBox(box, newDX, newDY, 5)) {
                    MoveComponent mc = new MoveComponent(tc.pos.x - (tc.pos.x - lc.xOffset) % lc.squareSize + lc.squareSize * 0.5f + newDX
                            , tc.pos.y - (tc.pos.y - lc.yOffset) % lc.squareSize + lc.squareSize * 0.5f + newDY, 5);
                    entity.add(mc);
                }
            } else if(lc.level.getObjectAt((int)((tc.pos.x - lc.xOffset + newDX) / lc.squareSize), lc.level.getHeight() - 1 - (int)((tc.pos.y - lc.yOffset + newDY) / lc.squareSize)) != 'w'){
                MoveComponent mc = new MoveComponent(tc.pos.x - (tc.pos.x - lc.xOffset) % lc.squareSize + lc.squareSize * 0.5f + newDX
                        , tc.pos.y - (tc.pos.y - lc.yOffset) % lc.squareSize + lc.squareSize * 0.5f + newDY, 5);
                entity.add(mc);
            }

            newTouch = false;
        }
    }

    public void setTouch(float x, float y) {
        LevelComponent lc = level.first().getComponent(LevelComponent.class);
        float tempX = x * multX;
        float tempY = (yOff - y) * multY;
        if(!detectUITouch(tempX, tempY)) {
            tempX += lc.xOffset;
            tempY -= lc.yOffset;
            tX = tempX;
            tY = tempY;
            newTouch = true;
        }
    }

    private boolean detectUITouch(float x, float y) {
        TransformComponent tc;
        for(Entity entity: uiElements) {
            tc = entity.getComponent(TransformComponent.class);
            if(tc != null) {
                if(Math.abs(tc.pos.x - x) < tc.width && Math.abs(tc.pos.y - y) < tc.height) {
                    UIComponent uiComponent = entity.getComponent(UIComponent.class);
                    if(uiComponent != null) {
                        switch (uiComponent.action) {
                            case UIComponent.ACTION_EXIT:
                                Gdx.app.exit();
                                break;
                            case UIComponent.ACTION_RETRY:
                                world.restart();
                                break;
                        }
                    }
                    return true;
                }
            }
        }
        return false;
    }

    private boolean pushBox(Entity box, float dx, float dy, int speed) {
        LevelComponent lc = level.first().getComponent(LevelComponent.class);
        TransformComponent tc = tm.get(box);
        Entity nextBox = getBoxAt(tc.pos.x + dx, tc.pos.y + dy);
        if(nextBox == null && lc.level.getObjectAt((int)((tc.pos.x - lc.xOffset + dx) / lc.squareSize), lc.level.getHeight() - 1 - (int)((tc.pos.y - lc.yOffset + dy) / lc.squareSize)) != 'w') {
            box.add(new MoveComponent(tc.pos.x + dx, tc.pos.y + dy, speed));
            return true;
        } else if(nextBox != null && pushBox(nextBox, dx, dy, speed)) {
            box.add(new MoveComponent(tc.pos.x + dx, tc.pos.y + dy, speed));
            return true;
        } else return false;
    }

    private Entity getBoxAt(float x, float y) {
        TransformComponent tf;
        for(Entity entity: boxes) {
            tf = entity.getComponent(TransformComponent.class);
            if (tf.pos.x == x && tf.pos.y == y) return entity;
        }
        return null;
    }
}
