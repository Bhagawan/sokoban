package com.beawergames.game.systems;

import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.badlogic.ashley.utils.ImmutableArray;
import com.beawergames.game.components.AnimationComponent;
import com.beawergames.game.components.BoxComponent;
import com.beawergames.game.components.TargetComponent;
import com.beawergames.game.components.TransformComponent;
import com.beawergames.game.util.Animations;

public class GeyserAnimSystem extends IteratingSystem {
    private ComponentMapper<TransformComponent> trm;
    private ImmutableArray<Entity> boxes;

    public GeyserAnimSystem(int priority) {
        super(Family.all(TargetComponent.class, TransformComponent.class).get(), priority);
        trm = ComponentMapper.getFor(TransformComponent.class);
    }

    @Override
    public void addedToEngine(Engine engine) {
        boxes = engine.getEntitiesFor(Family.all(BoxComponent.class).get());
        super.addedToEngine(engine);
    }

    @Override
    protected void processEntity(Entity entity, float deltaTime) {
        TransformComponent tc = trm.get(entity);
        Entity box = getBoxAt(tc.pos.x, tc.pos.y);
        if(box != null) entity.remove(AnimationComponent.class);
        else if(entity.getComponent(AnimationComponent.class) == null) entity.add(new AnimationComponent(Animations.geyser_anim));
    }

    private Entity getBoxAt(float x, float y) {
        TransformComponent tf;
        for(Entity entity: boxes) {
            tf = entity.getComponent(TransformComponent.class);
            if(tf != null){
                if (tf.pos.x == x && tf.pos.y == y) return entity;
            }
        }
        return null;
    }
}
