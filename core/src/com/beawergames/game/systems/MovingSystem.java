package com.beawergames.game.systems;

import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.badlogic.ashley.utils.ImmutableArray;
import com.badlogic.gdx.math.Vector3;
import com.beawergames.game.components.AnimationComponent;
import com.beawergames.game.components.BoxComponent;
import com.beawergames.game.components.LevelComponent;
import com.beawergames.game.components.MoveComponent;
import com.beawergames.game.components.TargetComponent;
import com.beawergames.game.components.TransformComponent;

public class MovingSystem extends IteratingSystem {
    private ComponentMapper<MoveComponent> mm;
    private ComponentMapper<TransformComponent> tm;
    private ImmutableArray<Entity> entities;

    private ImmutableArray<Entity> level;
    private ImmutableArray<Entity> boxes;
    private ImmutableArray<Entity> targets;

    private OnWinCallback callback;

    public MovingSystem(int priority) {
        super(Family.all(MoveComponent.class, TransformComponent.class).get(), priority);

        mm = ComponentMapper.getFor(MoveComponent.class);
        tm = ComponentMapper.getFor(TransformComponent.class);
    }

    @Override
    public void addedToEngine(Engine engine) {
        level = engine.getEntitiesFor(Family.all(LevelComponent.class).get());
        boxes = engine.getEntitiesFor(Family.all(BoxComponent.class).get());
        targets = engine.getEntitiesFor(Family.all(TargetComponent.class).get());
        super.addedToEngine(engine);
    }

    @Override
    protected void processEntity(Entity entity, float deltaTime) {
        MoveComponent mc = mm.get(entity);
        TransformComponent tc = tm.get(entity);
        LevelComponent lc = level.first().getComponent(LevelComponent.class);

        float dX = mc.endPoint.x - tc.pos.x;
        float dY = mc.endPoint.y - tc.pos.y;

        if(Math.abs(dY) < mc.speed && Math.abs(dX) < mc.speed) {
            tc.pos.x = mc.endPoint.x;
            tc.pos.y = mc.endPoint.y;
            entity.remove(MoveComponent.class);
            checkWin();
        } else {
            Vector3 moveV = new Vector3(mc.endPoint.x - tc.pos.x, mc.endPoint.y - tc.pos.y, 0).nor();
            tc.pos.mulAdd(moveV, mc.speed);
        }
    }

    public void setWinCallback(OnWinCallback callback) {
        this.callback = callback;
    }

    private void checkWin() {
        boolean win = true;
        for(Entity target : targets) {
            TransformComponent targetCoords = tm.get(target);
            boolean closed = false;
            for(Entity box : boxes) {
                TransformComponent boxCoords = tm.get(box);
                if(targetCoords.pos.y == boxCoords.pos.y && targetCoords.pos.x == boxCoords.pos.x) {
                    closed = true;
                    break;
                }
            }
            if(!closed) {
                win = false;
                break;
            }
        }
        if(win) if(callback != null) callback.onWin();
    }

    public interface OnWinCallback{
        void onWin();
    }
}
