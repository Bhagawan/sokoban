package com.beawergames.game.components;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class AnimationComponent implements Component {
    private final Animation<TextureRegion> animation;
    private float currFrameTime;

    public AnimationComponent( Animation<TextureRegion> animation ) {
        this.animation = animation;
        currFrameTime = 0;
    }

    public TextureRegion getCurrFrame(float deltaFrameTime) {
        currFrameTime += deltaFrameTime;
        if(currFrameTime > animation.getAnimationDuration()) currFrameTime = currFrameTime % animation.getFrameDuration();
        return animation.getKeyFrame(currFrameTime);
    }
}
