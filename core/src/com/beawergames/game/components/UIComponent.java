package com.beawergames.game.components;

import com.badlogic.ashley.core.Component;

public class UIComponent implements Component {
    public static final byte ACTION_EXIT = 0;
    public static final byte ACTION_RETRY = 1;

    public byte action;

    public UIComponent( byte action) {
        this.action = action;
    }
}
