package com.beawergames.game.components;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.math.Vector3;

public class TransformComponent  implements Component {
    public Vector3 pos = new Vector3();
    public float scaleX = 1.0f;
    public float scaleY = 1.0f;
    public float angle = 0.0f;
    public float width = 1.0f;
    public float height = 1.0f;

    public TransformComponent(float x, float y, float z) {
        pos.x = x;
        pos.y = y;
        pos.z = z;
    }

    public void setSize(float width, float height) {
        this.width = width;
        this.height = height;
    }

}
