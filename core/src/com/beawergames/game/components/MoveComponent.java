package com.beawergames.game.components;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.math.Vector3;

public class MoveComponent implements Component {
    public int speed;
    public Vector3 endPoint = new Vector3();

    public MoveComponent(float x, float y, int speed) {
        endPoint.x = x;
        endPoint.y = y;
        endPoint.z = 1;
        this.speed = speed;
    }


}
