package com.beawergames.game.components;

import com.badlogic.ashley.core.Component;
import com.beawergames.game.data.Level;

public class LevelComponent implements Component {

    public Level level;
    public float squareSize;
    public float xOffset = 0, yOffset = 0;

    public LevelComponent(Level level) {
        this.level = level;
    }
}
