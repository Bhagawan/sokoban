package com.beawergames.game.components;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class TextureComponent implements Component {
    public TextureRegion tex;

    public TextureComponent(TextureRegion tex) {
        this.tex = tex;
    }

    public TextureComponent(Texture tex) {
        this.tex = new TextureRegion(tex);
    }
}
