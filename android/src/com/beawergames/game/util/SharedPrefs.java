package com.beawergames.game.util;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedPrefs {
    public static void completeLevel(Context context, String name) {
        SharedPreferences shP = context.getSharedPreferences("completedLevels", Context.MODE_PRIVATE);
        shP.edit().putBoolean(name, true).apply();
    }

    public static boolean checkLevel(Context context, String name) {
        SharedPreferences shP = context.getSharedPreferences("completedLevels", Context.MODE_PRIVATE);
        return  shP.getBoolean(name, false);
    }
}
