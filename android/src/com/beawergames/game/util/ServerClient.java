package com.beawergames.game.util;

import com.beawergames.game.data.SplashResponse;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface ServerClient {

    @FormUrlEncoded
    @POST("Sokoban/splash.php")
    Call<SplashResponse> getSplash(@Field("locale")String locale);

    @GET("Sokoban/levels.txt")
    Call<ResponseBody> getLevels();

}
