package com.beawergames.game.util;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.beawergames.game.R;
import com.squareup.picasso.Picasso;

import java.util.List;

public class LevelsAdapter extends RecyclerView.Adapter<LevelsAdapter.ViewHolder> {
    private List<String> levels;
    private ItemClickListener mClickListener;

    public LevelsAdapter(List<String> levels) {
        this.levels = levels;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_level, parent, false);
        return new LevelsAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.name.setText(String.valueOf(position + 1));
        Picasso.get().load("http://195.201.125.8/Sokoban/level.png").resize(100, 100).into(holder.img);
        if(SharedPrefs.checkLevel(holder.itemView.getContext(), levels.get(position))) {
            holder.checkMark.setVisibility(View.VISIBLE);
            holder.img.setImageAlpha(80);
        } else {
            holder.checkMark.setVisibility(View.INVISIBLE);
            holder.img.setImageAlpha(255);
        }

    }

    @Override
    public int getItemCount() {
        if(levels != null) return levels.size();
        else return 0;
    }

    class ViewHolder extends RecyclerView.ViewHolder  implements View.OnClickListener{
        private TextView name;
        private ImageView img;
        private ImageView checkMark;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            img = itemView.findViewById(R.id.imageView_item);
            checkMark = itemView.findViewById(R.id.imageView_item_checkmark);
            name = itemView.findViewById(R.id.text_item_level_name);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (mClickListener != null) mClickListener.onItemClick(v, getAdapterPosition());
        }
    }

    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }

}
