package com.beawergames.game;

import android.os.Bundle;

import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import com.beawergames.game.data.Level;
import com.beawergames.game.util.SharedPrefs;

import java.util.ArrayList;

public class AndroidLauncher extends AndroidApplication {

	@Override
	protected void onCreate (Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();
		config.useImmersiveMode = true;
		ArrayList<String> level = getIntent().getStringArrayListExtra("level");
		String levelName = getIntent().getStringExtra("levelName");
		if(level != null && levelName != null) {
			Game game = new Game(new Level(level));
			game.setCallback(() -> SharedPrefs.completeLevel(getContext(), levelName));
			initialize(game, config);
		}
		else finish();
	}


}
