package com.beawergames.game;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.view.WindowCompat;
import androidx.core.view.WindowInsetsCompat;
import androidx.core.view.WindowInsetsControllerCompat;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

public class MainActivity extends AppCompatActivity {
    private Target target;

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);
        fullscreen();
        setBackground();
    }

    private void fullscreen() {
        WindowInsetsControllerCompat controllerCompat = new WindowInsetsControllerCompat(getWindow(), getWindow().getDecorView());
        controllerCompat.hide(WindowInsetsCompat.Type.systemBars());
        controllerCompat.setSystemBarsBehavior(WindowInsetsControllerCompat.BEHAVIOR_SHOW_TRANSIENT_BARS_BY_SWIPE);
        WindowCompat.setDecorFitsSystemWindows(getWindow(), false);
    }

    private void setBackground() {
        ConstraintLayout mainLayout = findViewById(R.id.layout_main);
        target = new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                mainLayout.setBackground(new BitmapDrawable(getResources(), bitmap));
            }

            @Override public void onBitmapFailed(Exception e, Drawable errorDrawable) { }

            @Override public void onPrepareLoad(Drawable placeHolderDrawable) { }
        };
        Picasso.get().load("http://195.201.125.8/Sokoban/back.png").into(target);
    }
}
