package com.beawergames.game.mvp;

import androidx.annotation.NonNull;

import com.beawergames.game.util.MyServerClient;
import com.beawergames.game.util.ServerClient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import moxy.InjectViewState;
import moxy.MvpPresenter;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@InjectViewState
public class LevelsPresenter extends MvpPresenter<LevelsPresenterViewInterface> {
    private Map<String, ArrayList<String>> levels = new HashMap<>();

    @Override
    protected void onFirstViewAttach() {
        Call<ResponseBody> call = MyServerClient.createService(ServerClient.class).getLevels();
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                if(response.isSuccessful() && response.body() != null) readLevels(response.body());
                else getViewState().serverError();
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                getViewState().serverError();
            }
        });
    }

    private void readLevels(ResponseBody body) {
        try ( InputStream is = body.byteStream() ) {
            BufferedReader br = new BufferedReader(new InputStreamReader(is));

            boolean newLevel = false;
            String currLevel = "";

            String line;
            while ((line = br.readLine()) != null) {
                if(line.equals("---")) newLevel = true;
                else if(newLevel) {
                    levels.put(line, new ArrayList<>());
                    currLevel = line;
                    newLevel = false;
                } else {
                    ArrayList<String> l = levels.get(currLevel);
                    if(l != null) l.add(line);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        ArrayList<String> out = new ArrayList<>(levels.keySet());
        Collections.sort(out, (o1, o2) -> {
            if (o1.length() == o2.length()){
                return o1.compareTo(o2);
            }
            return o1.length() - o2.length();
        });
        getViewState().fillLevels(out);
    }

    public void startLevel(String name) {
        if(levels.containsKey(name)) getViewState().startLevel(levels.get(name), name);
    }
}
