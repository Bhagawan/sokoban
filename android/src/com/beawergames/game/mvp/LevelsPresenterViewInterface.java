package com.beawergames.game.mvp;

import java.util.ArrayList;
import java.util.List;

import moxy.MvpView;
import moxy.viewstate.strategy.alias.OneExecution;
import moxy.viewstate.strategy.alias.SingleState;

public interface LevelsPresenterViewInterface extends MvpView {

    @SingleState
    void fillLevels(List<String> levels);

    @OneExecution
    void serverError();

    @OneExecution
    void startLevel(ArrayList<String> level, String levelName);
}
