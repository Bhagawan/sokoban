package com.beawergames.game.data;

import androidx.annotation.Keep;

import com.google.gson.annotations.SerializedName;

@Keep
public class SplashResponse {

    @SerializedName("url")
    private String url;

    public String getUrl() {
        return url;
    }
}
