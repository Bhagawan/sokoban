package com.beawergames.game.fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.beawergames.game.R;

public class MenuFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View mView = inflater.inflate(R.layout.fragment_menu, container, false);


        androidx.appcompat.widget.AppCompatButton newGameButton = mView.findViewById(R.id.btn_start_game);
        newGameButton.setOnClickListener(view -> requireActivity().getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fragmentContainerView, new LevelsFragment())
                .addToBackStack(null)
                .commit());

        androidx.appcompat.widget.AppCompatButton exitButton = mView.findViewById(R.id.btn_exit);
        exitButton.setOnClickListener(view -> requireActivity().finish());

        return mView;
    }
}