package com.beawergames.game.fragments;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Shader;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.beawergames.game.AndroidLauncher;
import com.beawergames.game.R;
import com.beawergames.game.mvp.LevelsPresenter;
import com.beawergames.game.mvp.LevelsPresenterViewInterface;
import com.beawergames.game.util.LevelsAdapter;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.util.ArrayList;
import java.util.List;

import moxy.MvpAppCompatFragment;
import moxy.presenter.InjectPresenter;

public class LevelsFragment extends MvpAppCompatFragment implements LevelsPresenterViewInterface {
    private View mView;
    private int reloadPosition = -1;
    private Target target;

    @InjectPresenter
    LevelsPresenter mPresenter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_levels, container, false);
        setBackground();

        androidx.appcompat.widget.AppCompatImageButton backButton = mView.findViewById(R.id.btn_levels_back);
        backButton.setOnClickListener(view -> requireActivity().getSupportFragmentManager().popBackStack());
        return mView;
    }

    @Override
    public void onResume() {
        RecyclerView recyclerView = mView.findViewById(R.id.recycler_levels);
        if(recyclerView.getAdapter() != null && reloadPosition != -1) {
            recyclerView.getAdapter().notifyItemChanged(reloadPosition);
            reloadPosition = -1;
        }
        super.onResume();
    }

    @Override
    public void fillLevels(List<String> levels) {
        RecyclerView recyclerView = mView.findViewById(R.id.recycler_levels);
        recyclerView.setLayoutManager(new GridLayoutManager(getContext(),3));
        LevelsAdapter adapter = new LevelsAdapter(levels);
        recyclerView.setAdapter(adapter);

        adapter.setClickListener((view, position) -> {
            mPresenter.startLevel(levels.get(position));
            reloadPosition = position;
        });
    }

    @Override
    public void serverError() {
        Toast.makeText(getContext(), getString(R.string.msg_error_server), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void startLevel(ArrayList<String> level, String levelName) {
        Intent intent = new Intent(getContext(), AndroidLauncher.class);
        intent.putStringArrayListExtra("level", level);
        intent.putExtra("levelName", levelName);
        startActivity(intent);
    }


    private void setBackground() {
        ConstraintLayout layoutLevels = mView.findViewById(R.id.layout_levels);
        target = new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                BitmapDrawable bitmapDrawable = new BitmapDrawable(getResources(), bitmap);
                bitmapDrawable.setTileModeXY(Shader.TileMode.REPEAT, Shader.TileMode.REPEAT);
                layoutLevels.setBackground(bitmapDrawable);
            }

            @Override public void onBitmapFailed(Exception e, Drawable errorDrawable) { }

            @Override public void onPrepareLoad(Drawable placeHolderDrawable) { }
        };
        Picasso.get().load("http://195.201.125.8/Sokoban/back_levels.png").resize(100, 100).into(target);
    }
}